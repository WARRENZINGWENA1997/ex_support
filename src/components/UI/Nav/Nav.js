import React, { useState, useEffect } from 'react';
import {Link } from 'react-router-dom'
import logoImage from '../../../assets/img/logo.jpeg';



const Nav = () => {
  
  const [navClass, setNavClass] = useState('');
  const [toggeledNav, settoggeledNav] = useState(false);

  const toggleNav = () => {
    settoggeledNav(!toggeledNav);
  };
  
  useEffect(() => {
    window.addEventListener('scroll', () => {
      let navClass = '';
      if (window.scrollY >= 200) {
        navClass = 'scrolled';
      }
      setNavClass(navClass);
    });
  }, []);
  return (
    <nav className={`navbar navbar-expand-md bg-light ${navClass}`}>
      <div className='container'>
        <div className='row'>
          <a className='navbar-brand' href='!#'>
            <div className='col-md-4 col-lg-2 mb-3'>
              <div className='d-flex'>
                <div className='mr-4'>
                  <img src={logoImage} alt='' />
                </div>
                <div className='brand-texts'>
                  <h2 className='brand-texts'>Exceptional</h2>
                  <h3 className='brand-texts'><span>Support</span> Services Ltd</h3>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div
          className={`navbar-toggler nav-icon ${(() => {
            if (toggeledNav) return 'open';
            return '';
          })()}`}
          onClick={toggleNav}>
          <span />
          <span />
          <span />
        </div>

        <div
          className={`collapse navbar-collapse ${(() => {
            if (toggeledNav) return 'show';
            return '';
          })()}`}
        >
          <ul className='navbar-nav ml-auto'>
            <li className='nav-item'>
              <Link to='/' offset={-120} className='nav-link' activeClassName="active">
                Home
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/about' className='nav-link'  activeClassName="active">
                About
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/services' className='nav-link'  activeClassName="active">
                Services
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/blog' className='nav-link'>
                Blog
              </Link>
            </li>
            <li className='nav-item'>
              <Link to='/contact' className='nav-link'>
                Contact
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
