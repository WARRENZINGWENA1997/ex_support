import React from 'react';

import Link from '../Link/Link';

const footer = () => {
  return (
    <footer className=''>
      <div className='container text-light pt-5'>
        <div className='row'>
          <div className='col-sm-6 col-md-6 col-lg-4 mb-5'>
            <div className='footer-title'>
              <h6>About Us</h6>
            </div>
            <div className='footer-content'>
              <p>
                <small className='text-muted'>
                  Exceptional Support Service provides community care
                  for children and adults, as well as child day care,
                  before/after school, and holiday care, as well as private tutoring at all levels,
                  from elementary to university, and professional social work services.
                </small>
              </p>
              <a href='#!' className='btn btn-sm btn-primary'>
                Read More
              </a>
            </div>
          </div>
          <div className='col-sm-6 col-md-6 col-lg-2 mb-5'>
            <div className='footer-title'>
              <h6>Quick Links</h6>
            </div>
            <div className='footer-content'>
              <ul className='list-group quick-links'>
                <li>
                  <Link target='home' offset={-120}>
                    Home
                  </Link>
                </li>
                <li>
                  <Link target='about'>About</Link>
                </li>
                <li>
                  <Link target='services'>Services</Link>
                </li>
                <li>
                  <Link target='blog'>Blog</Link>
                </li>
                <li>
                  <Link target='contact'>Contact</Link>
                </li>
              </ul>
            </div>
          </div>
          <div className='col-sm-6 col-md-6 col-lg-3 mb-5'>
            <div className='footer-title'>
              <h6>Latest News</h6>
            </div>
            <div className='footer-content'>
              <p>
                <small className='text-muted'>
                  1. Thousands fewer receiving care
                </small>
              </p>
              <p>
                <small className='text-muted'>
                  2. Care homes have suffered during the pandemic
                </small>
              </p>
              <p>
                <small className='text-muted'>3. Spending is lower than 2010</small>
              </p>
              <p>
                <small className='text-muted'>
                  4. Fees for care differ across England
                </small>
              </p>
              <a href='https://www.independent.co.uk/topic/social-care' className='btn btn-sm btn-primary'>
                Read More
              </a>
            </div>
          </div>
          <div className='col-sm-6 col-md-6 col-lg-3 mb-5'>
            <div className='footer-title'>
              <h6>Contact Us</h6>
            </div>
            <div className='footer-content'>
              <p className='text-muted'>
                <small>Address : 119 Fairview Estate ,Harare Zimbabwe</small>
              </p>
              <p className='text-muted'>
                <small>Phone : +44 7915 651180</small>
              </p>
              <p className='text-muted'>
                <small>E-mail : contact@email.com</small>
              </p>
              <div className='social-media mt-4'>
                <a href='!#' className='text-light'>
                  <i className='fab fa-facebook-f mr-4' />
                </a>
                <a href='!#' className='text-light'>
                  <i className='fab fa-twitter mr-4' />
                </a>
                <a href='!#' className='text-light'>
                  <i className='fab fa-instagram mr-4' />
                </a>
                <a href='https://wa.me/+447915651180' className='text-light'>
                  <i className='fab fa-whatsapp' />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='bottom-footer pt-3 pb-3 text-center'>
        <small>© All Right Reserved. Design Wz Softwares</small>
      </div>
    </footer>
  );
};

export default footer;
