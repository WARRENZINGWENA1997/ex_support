import React from 'react';
import Header from '../UI/Header/Header';
import Footer from '../UI/Footer/Footer';
import Home from '../sections/Home/Home';
import About from '../sections/About/About';
import Service from '../sections/Service/Service';
import Facts from '../sections/Facts/Facts';
import Blog from '../sections/Blog/Blog';
import Contact from '../sections/Contact/Contact';
import MapMarker from '../sections/MapMarker/MapMarker';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

const Layout = () => {
  return (
    <Router>
      <div>
        <Header />
        <main>

        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/services" element={<Service />} />
            <Route path="/blog" element={<Blog />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/services/tutorials" element={<Blog />} />

            </Routes>
            </main>

        <Facts />
        <MapMarker />
        <Footer />
      </div>
    </Router>

  );
};

export default Layout;
