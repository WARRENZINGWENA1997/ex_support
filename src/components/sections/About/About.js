import React from 'react';

import Section from '../../../HOC/Section';
import aboutImage from '../../../assets/img/about5.jpeg';
import ParticlesBg from 'particles-bg'

const about = () => {
  return (
    <Section id='about'>
      <div className='aboutdiv container pt-2 pb-5'  style={{ backgroundImage: `url(${<ParticlesBg type="circle" bg={true} />})`}}>

        <div className='section-header pt-5 pb-5 text-center'>
          <h3 className='section-title'> Our Company</h3>
          <div className='section-subtitle mr-auto ml-auto'>
            <h6 >
              Exceptional Support Service provides community care for children and adults,
              as well as child day care, before/after school, and holiday care, as well as private tutoring at all levels,
              from elementary to university, and professional social work services.
            </h6>
          </div>
        </div>


        <div className='section-content'>

          <div className='row'>

            <div className='col-md-12 col-lg-6 mb-3'>
              <div className='aboutImage'>
                <img src={aboutImage} alt='about company' />
              </div>
            </div>

            <div className='col-md-12 col-lg-6'>
              <h3 className='about-title'>About us</h3>

              <div className='about-description'>
                <p>
                  We specialize in social care services that range from physical and mental
                  health through deploying our skilled care professionals into vulnerable people's homes to
                  assist them with healthy eating, medicine, counseling, and socialization..
                </p>
                <p>
                  We also provide private tutoring ranging from elementary school through early
                  childhood development, as well as holiday lessons for students
                  who need to prepare for examinations or get ahead on their class syllabus.
                </p>
                <button className='btn btn-primary  rounded-5'>Read More</button>
              </div>
            </div>


          </div>

        </div>
      </div>
    </Section>
  );
};

export default about;
