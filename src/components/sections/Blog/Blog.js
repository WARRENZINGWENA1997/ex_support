import React from 'react';

import Section from '../../../HOC/Section';

import blogImage1 from '../../../assets/img/blog11.png';
import blogImage2 from '../../../assets/img/blog22.jpg';
import blogImage3 from '../../../assets/img/blog33.jpg';

const Blog = () => {
  return (
    <Section id='blog'>
      <div className='container pt-2 pb-5'>
        <div className='section-header pt-5 pb-5 text-center'>
          <h3 className='section-title'>
            <span>Exceptional</span> <span>Support </span>Blog
          </h3>
          <h6 className='section-subtitle mr-auto ml-auto'>
          Welcome to our blog where we discuss current events
           and interesting subjects in the community care market.
          Caregivers and home care managers finds the following topics of interest.
          </h6>
        </div>
        <div className='section-content'>
          <div className='row'>
            <div className='col-lg-4 mb-3'>
              <div className='card rounded-0'>
                <img src={blogImage1} className='card-img-top' alt='Blog 1' />
                <div className='card-body'>
                  <h5 className='card-title'>How to recruit and retain great care staff</h5>
                  <p className='card-text'>
                  Staff recruitment and retention remain long-standing challenges 
                  for the care sector, with the coronavirus pandemic adding to the 
                  difficulties many providers face. Cygnet Healthcare's Fiona Oxley
                   presents an uplifting and positive take on 
                  recruiting great colleagues - and keeping them.....
                  </p>
                  <a href='https://socialcare.blog.gov.uk/2021/10/14/how-to-recruit-and-retain-great-care-staff/' className='btn btn-sm btn-primary'>
                    Read More
                  </a>
                </div>
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <div className='card rounded-0'>
                <img src={blogImage2} className='card-img-top' alt='Blog 2' />
                <div className='card-body'>
                  <h5 className='card-title'>Why I decided to take the COVID-19 vaccine</h5>
                  <p className='card-text'>
                  "My name is Sofija Igonkina, from Latvia. I have been living in England for 15 years.
                   I'm a nurse and I work in Evergreen nursing home in Hastings. I want to share my story about why 
                  I had the COVID-19 vaccine and why I'm going to get a booster jab this winter....."
                  </p>
                  <a href='#!' className='btn btn-sm btn-primary'>
                    Read More
                  </a>
                </div>
              </div>
            </div>
            <div className='col-lg-4 mb-3'>
              <div className='card rounded-0'>
                <img src={blogImage3} className='card-img-top' alt='Blog 3' />
                <div className='card-body'>
                  <h5 className='card-title'>Social care recruitment and the COVID-19 pandemic</h5>
                  <p className='card-text'>
                  The COVID-19 pandemic has presented challenges for many colleagues working in the social care sector, making retention and recruitment more important than ever. Vida Healthcare's Bernadette Mossman explains
                   how she and her colleagues have been encouraging vaccination among their staff.....
                  </p>
                  <a href='https://socialcare.blog.gov.uk/2021/09/02/social-care-recruitment-and-the-covid-19-pandemic/' className='btn btn-sm btn-primary'>
                    Read More
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Blog;
