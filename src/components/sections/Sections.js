import React, { Fragment } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

const sections = () => {
  return (
  <Router>
    <Fragment>
      <Home />
      <About />
      <Service />
      <Blog />
      <Facts />
      <Contact />
      <MapMarker />
    </Fragment>
    </Router>
  );
  
};

export default sections;
