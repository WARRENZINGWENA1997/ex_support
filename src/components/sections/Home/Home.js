import React from 'react';
import Section from '../../../HOC/Section';
import homeImage1 from '../../../assets/img/about5.jpeg';
import homeImage2 from '../../../assets/img/home3.jpg';
import homeImage3 from '../../../assets/img/tutor1.jpg';
import homeImage4 from '../../../assets/img/ff.webp';
import Link from '../../UI/Link/Link';

const images = [
  `url(${homeImage1})`, `url(${homeImage2})`, `url(${homeImage3})`, `url(${homeImage4})`
]
const delay = 3500;

export default function Home() {
  const [index, setIndex] = React.useState(0);
  const timeoutRef = React.useRef(null);

  function resetTimeout() {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
  }

  React.useEffect(() => {
    resetTimeout();
    timeoutRef.current = setTimeout(
      () =>
        setIndex((prevIndex) =>
          prevIndex === images.length - 1 ? 0 : prevIndex + 1
        ),
      delay
    );

    return () => {
      resetTimeout();
    };
  }, [index]);

  return (
    <Section id="home">
      <div className="slideshow">
        <div className="slideshowSlider" style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}>
          {images.map((imageValues, index) => (
            <div className="slide"
              key={index}
              style={{ backgroundImage: imageValues }}>
              <div className='intro container text-center text-black'>
                <h2 className='welcome mb-4'>Welcome to</h2>
                <h1 className='title'>Exceptional Support Services Ltd</h1>
                <p className='sub-title mb-3'>
                  Exceptional Support Service offers the finest level of home care in
                  London, as well as the best customer service in the industry. </p>
                <Link target='about' classes=' btn btn-primary rounded-6 mr-2'>
                  Learn More
                </Link>
                <Link target='contact' classes='btn btn-default text-white rounded-6'>
                  Contact Us
                </Link>

              </div>
            </div>
          ))}
        </div>

        <div className="slideshowDots">
          {images.map((_, idx) => (
            <div
              key={idx}
              className={`slideshowDot${index === idx ? " active" : ""}`}
              onClick={() => {
                setIndex(idx);
              }}
            ></div>
          ))}
        </div>
      </div>
    </Section>
  );
}
