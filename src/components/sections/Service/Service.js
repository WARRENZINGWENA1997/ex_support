import React from 'react';
import Section from '../../../HOC/Section';
import serviceImage1 from '../../../assets/img/45.jpg';
import serviceImage2 from '../../../assets/img/tutor2.jpg';
import serviceImage3 from '../../../assets/img/childcare.jpg';
import { Link } from 'react-router-dom';



export default function Service() {
  return (


    <Section id='services'>
      <div>

        <div className='service-content container pt-2 pb-5'>
          <div className='section-header pt-5 pb-5 text-center'>
            <h3 className='section-title'>
              <span>Our </span>Services
            </h3>
            <h6 className='section-subtitle mr-auto ml-auto'>
              Individualized quality care that meets the total needs of the
              of the community ,which ranges from the childhood, middle age , and old age
            </h6>
          </div>
          <div className='section-content'>
            <div className='row'>
              <div class="col-md-6 col-lg-4 mb-3">
                <div class="service-body card-header text-center">
                  <Link to ='/services/tutorials'><h5 className='service-title'>Private Tutoring</h5></Link>
                </div>
                <img class="card-img" src={serviceImage2} alt="Card cap" />
                <div class="card-texts text-start">
                  <p className='service-description'>
                    Provides students with individualized instruction and offer specialized skills, methods,
                    or approaches to meet their specific learning requirements
                  </p>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 mb-3">
                <div class="service-body card-header text-center">

                  <h5 className='service-title'>Adult Care</h5>
                </div>
                <img class="card-img" src={serviceImage1} alt="Card  cap" />
                <div class="card-texts text-start">
                  <p className='service-description'>
                    Offering evidence-based treatments to individuals with
                    significant mental illness of all ages
                  </p>
                </div>
              </div>

              <div class="col-md-6 col-lg-4 mb-3">
                <div class="service-body card-header text-center">
                  <h5 className='service-title'>Child Day Care</h5>
                </div>
                <img class="card-img" src={serviceImage3} alt="Card  cap" />
                <div class="card-texts text-starts">
                  <p className='service-description'>
                    Early childhood care is becoming more prevalent,
                    and it plays a crucial part in children's development as well as providing valuable support to families with small children.
                    We offer individualized children care, as well
                    as before/after school and holiday care.
                  </p>
                </div>
                <div>

                </div>
              </div>
            </div>
          </div>

        </div>

      </div>

    </Section>
  );
}


